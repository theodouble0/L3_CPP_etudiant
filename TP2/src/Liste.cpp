#include "Liste.hpp"

#include <iostream>

Liste::Liste():
_tete(nullptr){
}

void Liste::ajouterDevant(int valeur){
    this->_tete = new Noeud{valeur, _tete};
}

int Liste::getTaille() const{
	int cmp = 0;
	Noeud * tmp = this->_tete;
    while(tmp){
		cmp++;
		tmp = tmp->_suivant;
    }
    return cmp;
}

int Liste::getElement(int indice) const{
	if(indice > (this->getTaille()-1) or indice <0)
		return -1;
	Noeud* tmp = this->_tete;
	for( int i = 0; i<indice; i++){
		tmp = tmp->_suivant;
	}	
	return tmp->_valeur;
}

Liste::~Liste(){
   if(this->_tete !=nullptr){
       Noeud * tmp = this->_tete;
       Noeud * tmp1 = nullptr;
       while(tmp->_suivant != nullptr){
           tmp1 = tmp;
           tmp = tmp->_suivant;
           delete tmp1;
       }
       delete tmp;
    }else
   delete _tete;
}
