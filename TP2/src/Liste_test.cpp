#include "Liste.hpp"
#include <CppUTest/CommandLineTestRunner.h>

#include <iostream>

TEST_GROUP(GroupListe) {
};

TEST(GroupListe, liste_vide){
	Liste lst;
	CHECK_EQUAL(0, lst.getTaille())
    CHECK_EQUAL(-1, lst.getElement(2))
}


TEST(GroupListe, liste_getElement){
    Liste lst;
    lst.ajouterDevant(5);
    lst.ajouterDevant(10);
    lst.ajouterDevant(15);
    lst.ajouterDevant(20);
    lst.ajouterDevant(25);
	CHECK_EQUAL(5, lst.getElement(4));
}

TEST(GroupListe, liste_taille){
    Liste lst;
    lst.ajouterDevant(5);
    lst.ajouterDevant(10);
    lst.ajouterDevant(15);
    lst.ajouterDevant(20);
    lst.ajouterDevant(25);
    CHECK_EQUAL(5, lst.getTaille());
}

