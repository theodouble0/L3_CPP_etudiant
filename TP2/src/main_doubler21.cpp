#include "Doubler.hpp"
#include "Liste.hpp"

#include <iostream>

int main() {
    /*std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;
    
    //SCENARIO A
    int a = 42;
    int* p = &a;
    //~ std::cout << a <<std::endl;
    *p = 37;
    //~ std::cout << a <<std::endl;


    //SCENARIO B
    int* t;
    t = new int[10];
    t[2] = 42;
    std::cout << "t[2] :" << t[2] <<std::endl;
    delete[] t; //on peut continuer d'utiliser t
    std::cout << "t[2] :" << t[2] <<std::endl;
    t = nullptr;*/
    
    Liste lst;
    lst.ajouterDevant(37);
    lst.ajouterDevant(13);
    lst.ajouterDevant(55);
    lst.ajouterDevant(555);
    for(int i=0; i<lst.getTaille(); i++)
		std::cout << lst.getElement(i) << "->";
    std::cout << std::endl;
    return 0;
}

