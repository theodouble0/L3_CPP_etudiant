#include "ZoneDessin.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

ZoneDessin::ZoneDessin() :
	_engine(std::random_device{}()),
	_distRayon(20,80),
	_distCotes(3,8)
{
    _figures.push_back(new Ligne({1,0,0}, {10,10}, {630,10}));
    _figures.push_back(new Ligne({1,0,0}, {630,10}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,470}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,10}, {10,470}));
    _figures.push_back(new PolygoneRegulier({0,1,0}, {100,200}, 50, 5));
}

ZoneDessin::~ZoneDessin(){
	for (auto x : _figures) {
		delete x;
		x=nullptr;
	}
}

bool ZoneDessin::on_expose_event(GdkEventExpose* event){
	
}

bool ZoneDessin::gererClic( GdkEventButton* eb){
	if(eb->button == 1){
		int rayon = _distRayon(_engine);
		int cotes = _distCotes(_engine);
		_figures.push_back(new PolygoneRegulier({0,1,0}, {eb->x,eb->y},rayon,cotes));
	}
	else if(eb->button == 3){
		if (not _figures.empty()) {
			_figures.pop_back();
		}
	}
	
	auto win = get_window();
	win -> invalidate(true);
	return true;
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context){
	context->set_source_rgb(1.0, 0.0, 0.0);
	context->set_line_width(10.0);
	
	auto window = get_window();
	context->move_to(0,0);
	context->line_to(window->get_width(), window->get_height());
	
	for(FigureGeometrique * figure : _figures){
	  figure->afficher(context);
	}
	
	context->stroke();
	return true;
}
