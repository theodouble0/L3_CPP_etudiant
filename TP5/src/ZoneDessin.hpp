#ifndef ZONEDESSIN_HPP
#define ZONEDESSIN_HPP

#include <gtkmm.h>
#include <vector>
#include <random>
#include "FigureGeometrique.hpp"

class ZoneDessin : public Gtk::DrawingArea{
	private:
		std::vector<FigureGeometrique*> _figures;
		std::mt19937 _engine;
		std::uniform_int_distribution<int> _distRayon;
		std::uniform_int_distribution<int> _distCotes;
	public:
		bool on_draw(const Cairo::RefPtr<Cairo::Context> &context) override;
		ZoneDessin();
		~ZoneDessin();
		bool on_expose_event(GdkEventExpose* event);
		bool gererClic( GdkEventButton* eb);
};

#endif // DRAWINGAREA_HPP
