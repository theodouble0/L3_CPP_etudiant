#include <iostream>
#include <cmath>

#include "PolygoneRegulier.hpp"

#define _USE_MATH_DEFINES

PolygoneRegulier::PolygoneRegulier(
        const Couleur & couleur,
        const Point & centre,
        int rayon, int nbCotes):
    FigureGeometrique(couleur), _nbPoints(nbCotes){
    this->_points = new Point[nbCotes];
    double angle = 2*M_PI/double(nbCotes);
    for(int i=0; i<nbCotes; i++){
        this->_points[i]._x = double(rayon) * cos(angle*double(i)) + double(centre._x);
        this->_points[i]._y = double(rayon) * sin(angle*double(i)) + double(centre._y);
    }
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context) const {

    const Couleur & c = getCouleur();
    context->set_source_rgb(c._r, c._g, c._b);

    const Point & p1 = _points[_nbPoints-1];
    context->move_to(p1._x, p1._y);

    for (int i=0; i<_nbPoints; ++i) {
        const Point & p = _points[i];
        context->line_to(p._x, p._y);
    }

    context->stroke();

}


int PolygoneRegulier::getNbPoints() const{
    return this->_nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const{
    return this->_points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
    delete[] _points;
}
