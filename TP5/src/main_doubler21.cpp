#include "Doubler.hpp"
#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "Couleur.hpp"
#include "Point.hpp"
#include "PolygoneRegulier.hpp"
#include "ViewerFigures.hpp"
#include <iostream>
#include <vector>
#include <gtkmm.h>

int main(int argc, char ** argv) {
    /*Couleur c{0,0,0};

    Point p2{100,200};

    std::vector<FigureGeometrique*> figures {
        new Ligne({1,0,0}, {1, 0}, {0, 1}),
        new PolygoneRegulier(c, {100, 200}, 50, 5)
    };
    for (FigureGeometrique * f : figures)
        f->afficher();
    for (FigureGeometrique * f : figures)
        delete f;*/
    ViewerFigures test(argc, argv);
    test.run();
    return 0;
}

