#include <iostream>
#include <cmath>

#include "PolygoneRegulier.hpp"

#define _USE_MATH_DEFINES

PolygoneRegulier::PolygoneRegulier(
        const Couleur & couleur,
        const Point & centre,
        int rayon, int nbCotes):
    FigureGeometrique(couleur), _nbPoints(nbCotes){
    this->_points = new Point[nbCotes];
    double angle = 2*M_PI/double(nbCotes);
    for(int i=0; i<nbCotes; i++){
        this->_points[i]._x = double(rayon) * cos(angle*double(i)) + double(centre._x);
        this->_points[i]._y = double(rayon) * sin(angle*double(i)) + double(centre._y);
    }
}

void PolygoneRegulier::afficher() const{
    std::cout << "PolygoneRegulier " << this->getCouleur()._r << "_" << this->getCouleur()._g << "_" << this->getCouleur()._b << " " ;
    for(int i = 0; i< this->_nbPoints; i++)
        std::cout << this->_points[i]._x << "_" << this->_points[i]._y << " ";
    std::cout << std::endl;
}


int PolygoneRegulier::getNbPoints() const{
    return this->_nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const{
    return this->_points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
    delete[] _points;
}
