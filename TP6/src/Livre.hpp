#ifndef LIVRE_HPP
#define LIVRE_HPP

#include <iostream>

class Livre{
	private:
		std::string _titre;
		std::string _auteur;
		int _annee;
	public:
		Livre();
		Livre(const std::string & titre, const std::string & auteur, int annee);
		const std::string & getTitre() const;
		const std::string & getAuteur() const;
		int getAnnee() const;
		bool operator<(Livre const & a);
		bool operator==(Livre const & a);
	friend std::ostream& operator<<(std::ostream&, const Livre&);
	friend std::istream& operator>>(std::istream&, Livre&);
};

#endif // BIBLIOTHEQUE_HPP
