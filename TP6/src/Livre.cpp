#include "Livre.hpp"
#include <iostream>
#include <string>
Livre::Livre(){}


Livre::Livre(const std::string & titre, const std::string & auteur, int annee){
	if(titre.find(";")!= std::string::npos)
		throw(std::string("erreur : titre non valide (';' non autorisé)"));
	if(titre.find("\n")!= std::string::npos)
		throw(std::string("erreur : titre non valide ('\n' non autorisé)"));
	if(auteur.find(";")!= std::string::npos)
		throw(std::string("erreur : auteur non valide (';' non autorisé)"));
	if(auteur.find("\n")!= std::string::npos)
		throw(std::string("erreur : auteur non valide ('\n' non autorisé)"));
	_titre = titre;
	_auteur = auteur;
	_annee = annee;
}

const std::string & Livre::getTitre() const{
	return _titre;
}

const std::string & Livre::getAuteur() const{
	return _auteur;
}

int Livre::getAnnee() const{
	return _annee;
}

bool Livre::operator<(Livre const & a){
	if(_auteur < a.getAuteur())
		return true;
	else if(_auteur == a.getAuteur() && _titre < a.getTitre())
		return true;
	else
		return false;
}

bool Livre::operator==(Livre const & a){
	if(_auteur == a.getAuteur() && _titre == a.getTitre() && _annee == a.getAnnee())
		return true;
	else
		return false;
}

std::ostream& operator<<(std::ostream& os, const Livre& l) {
    os << l.getTitre() << ";" << l.getAuteur() << ";" << l.getAnnee();
}



std::istream & operator>>(std::istream& s, Livre& l) {
	std::getline(s, l._titre, ';');
	std::getline(s, l._auteur, ';');
	std::string chaine;
	std::getline(s, chaine, ';');
	l._annee = std::stoi(chaine);
	return s;
}

