#include "Bibliotheque.hpp"
#include <algorithm>
#include <fstream>
#include <sstream>

void Bibliotheque::afficher() const{
	for(Livre l : *this){
		std::cout << l << std::endl;
	}
	std::cout << std::endl;
}

void Bibliotheque::trierParAuteurEtTitre(){
	std::sort(begin(), end());
}

void Bibliotheque::trierParAnnee(){		
	auto fAnnee = [](const Livre& l1, const Livre& l2){
		return l1.getAnnee()<l2.getAnnee();};
	std::sort(begin(), end(), fAnnee);
}

void Bibliotheque::ecrireFichier(const std::string & nomFichier) const{
	std::ofstream os(nomFichier);
	if(os.is_open()){
		for(Livre l : *this){
			os << l << std::endl;
		}
	}
}

void Bibliotheque::lireFichier(const std::string & nomFichier){
	std::ifstream is(nomFichier);
	if(is.is_open()){
		std::string line;
		Livre tmp;
		while(std::getline(is, line)){
			std::stringstream s(line);
			s >> tmp;
			push_back(tmp);
		}
		is.close();
	} else
		throw(std::string("erreur : lecture du fichier impossible"));
}
