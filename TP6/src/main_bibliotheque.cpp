#include <iostream>

#include "Bibliotheque.hpp"
#include "Livre.hpp"

int main() {
    Bibliotheque test;
    test.push_back(Livre("a", "a", 1));
    test.push_back(Livre("b", "b", 2));
    test.push_back(Livre("d", "d", 4));
    test.push_back(Livre("c", "c", 3));
    test.afficher();
    test.trierParAuteurEtTitre();
    test.afficher();
    return 0;
}

