#include "Inventaire.hpp"
#include <algorithm>
#include <functional>

void Inventaire::trier(){
	auto cmp = [](const Bouteille & b1, const Bouteille & b2) -> bool { return b1._nom < b2._nom; };
	std::sort(_bouteilles.begin(), _bouteilles.end(), cmp);
}

std::ostream & operator <<(std::ostream & os, const Inventaire & in){
	auto f = [&os](const Bouteille &b) {
		os << b;
	};
	std::for_each(in._bouteilles.begin(), in._bouteilles.end(), f);
	
	
	/*for (const Bouteille & b : in._bouteilles)	
		os << b;*/
	return os;
}

std::istream & operator>>(std::istream & is, Inventaire & in){

	Bouteille b;

	while(is >> b)	{
		in._bouteilles.push_back(b);
	}

	return is;
}
