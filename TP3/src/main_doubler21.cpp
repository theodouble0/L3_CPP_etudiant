#include <iostream>

#include "Magasin.hpp"
#include "Doubler.hpp"
#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"

int main() {
    /*Location * loc = new Location{0,2};
    loc->afficherLocation();

    Client * cli = new Client(1, "théo");
    cli->afficherClient();


    Produit * pro = new Produit(1, "une pomme");
    pro->afficherProduit();*/
    
    Magasin mag;
    mag.ajouterClient("théo");
    mag.ajouterClient("pierre");
    mag.ajouterClient("antoine");

    mag.ajouterProduit("test1");
    mag.ajouterProduit("test2");
    mag.ajouterProduit("test3");

    try{
        mag.ajouterLocation(1, 1);
        mag.ajouterLocation(1, 2);
        mag.ajouterLocation(2, 0);
        mag.ajouterLocation(0, 0);
    }catch(const std::string  & s){
        std::cerr << s << std::endl;
    }
    try{
        mag.supprimerClient(10);
        mag.supprimerProduit(1);
    }catch(const std::string  & s){
        std::cerr << s << std::endl;
    }
    mag.afficherClients();
    std::cout << std::endl;
    mag.afficherProduits();
    std::cout << std::endl;
    mag.afficherLocations();
    std::cout << "nb produit libre: " << mag.calculerProduitLibres().size() << "    " << "nb client libre: " << mag.calculerClientLibres().size() << std::endl;
    return 0;
}

