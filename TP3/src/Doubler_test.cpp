#include "Doubler.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupDoubler) { };

TEST(GroupDoubler, Doubler_test1)  {
    CHECK_EQUAL(42, doubler(21));
}

TEST(GroupDoubler, Doubler_test2)  {
    CHECK_EQUAL(0, doubler(0));
}

