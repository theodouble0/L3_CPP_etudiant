#include "Magasin.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, Magasin_ajouter_nbClients)  {
    Magasin mag;
    mag.ajouterClient("théo");
    mag.ajouterClient("pierre");
    CHECK_EQUAL(2, mag.nbClients());
}

TEST(GroupMagasin, Magasin_supprimerClients)  {
    Magasin mag;
    mag.ajouterClient("théo");
    mag.ajouterClient("pierre");
    mag.supprimerClient(0);
    CHECK_EQUAL(1, mag.nbClients());
}

TEST(GroupMagasin, Magasin_supprimerClientsException)  {
    Magasin mag;
    mag.ajouterClient("théo");
    mag.ajouterClient("pierre");
    try{
        mag.supprimerClient(10);
        CHECK(false);
    }catch(const std::string & s){
        CHECK(true);
    }
}

TEST(GroupMagasin, Magasin_ajouter_nbProduits)  {
    Magasin mag;
    mag.ajouterProduit("Test1");
    mag.ajouterProduit("Test2");
    CHECK_EQUAL(2, mag.nbProduits());
}

TEST(GroupMagasin, Magasin_supprimerProduits)  {
    Magasin mag;
    mag.ajouterProduit("Test1");
    mag.ajouterProduit("Test2");
    mag.supprimerProduit(0);
    CHECK_EQUAL(1, mag.nbProduits());
}

TEST(GroupMagasin, Magasin_supprimerProduitsExceptions)  {
    Magasin mag;
    mag.ajouterProduit("Test1");
    mag.ajouterProduit("Test2");
    try{
        mag.supprimerProduit(10);
        CHECK(false);
    }catch(std::string s){
        CHECK(true);
    }
}
