#include <iostream>

#include "Client.hpp"

Client::Client(int id, const std::string& nom):
    _id(id), _nom(nom){

}

int Client::getId() const{
    return this->_id;
}

const std::string & Client::getNom() const{
    return this->_nom;
}

void Client::afficherClient() const{
    std::cout << "Client (" << this->getId() << ", " << this->getNom() <<  ")" << std::endl;
}
