#include <CppUTest/CommandLineTestRunner.h>
#include <iostream>

#include "Produit.hpp"

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, Client_getid)  {
    Produit pro = Produit(1, "une pomme");
    CHECK_EQUAL(pro.getId(), 1);
}

TEST(GroupProduit, Doubler_getNom)  {
    Produit pro = Produit(1, "une pomme");
    CHECK_EQUAL(pro.getDescription(), "une pomme");
}

