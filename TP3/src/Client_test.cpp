#include <CppUTest/CommandLineTestRunner.h>
#include <iostream>

#include "Client.hpp"

TEST_GROUP(GroupClient) { };


TEST(GroupClient, Client_getid)  {
    Client cli = Client(1, "Théo");
    CHECK_EQUAL(1, cli.getId());
}

TEST(GroupClient, Doubler_getNom)  {
    Client cli = Client(1, "Théo");
    CHECK_EQUAL("Théo" , cli.getNom());
}

