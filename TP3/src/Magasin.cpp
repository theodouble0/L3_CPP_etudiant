#include <iostream>
#include <vector>
#include "Magasin.hpp"


Magasin::Magasin():
    _idCourantClient(0), _idCourantProduit(0){
}

int Magasin::nbClients() const{
	return this->_clients.size();
}

void Magasin::ajouterClient(const std::string & nom){
	this->_clients.push_back(Client(this->_idCourantClient, nom));
	this->_idCourantClient++;
}

void Magasin::afficherClients() const{
	for(int i=0; i<this->nbClients(); i++)
		std::cout << this->_clients[i].getId() << ": " << this->_clients[i].getNom() << std::endl;
}

void Magasin::supprimerClient(int idClient){
    for(int i=0; i< this->nbClients(); i++){
        if(this->_clients[i].getId() == idClient){
            std::swap(this->_clients[i], this->_clients[this->nbClients()-1]);
            this->_clients.pop_back();
            return;
        }
    }
    throw std::string("erreur: le client "+std::to_string(idClient)+ " n'existe pas");
}

int Magasin::nbProduits() const{
    return this->_produits.size();
}

void Magasin::ajouterProduit(const std::string & produit){
    this->_produits.push_back(Produit(this->_idCourantProduit, produit));
    this->_idCourantProduit++;
}

void Magasin::afficherProduits() const{
    for(int i=0; i<this->nbProduits(); i++)
        std::cout << this->_produits[i].getId() << ": " << this->_produits[i].getDescription() << std::endl;
}

void Magasin::supprimerProduit(int idProduit){
    for(int i=0; i< this->nbProduits(); i++){
        if(this->_produits[i].getId() == idProduit){
            std::swap(this->_produits[i], this->_produits[this->nbProduits()-1]);
            this->_produits.pop_back();
            return;
        }
    }
    throw std::string("erreur: le produit "+std::to_string(idProduit)+" n'existe pas");
}

int Magasin::nbLocations() const{
    return this->_locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit){
    if(!this->clientPresent(idClient))
        throw std::string("erreur: le Client "+std::to_string(idClient)+" n'existe pas");
    if(this->trouverProduitDansLocation(idProduit))
        throw std::string("erreur: le produit "+std::to_string(idProduit)+" est déja loué");
    this->_locations.push_back(Location{idClient, idProduit});
}

void Magasin::afficherLocations() const{
    for(int i=0; i<this->nbLocations(); i++)
        std::cout << this->_locations[i]._idClient << ": " << this->_locations[i]._idProduit << std::endl;
}

void Magasin::supprimerLocation(int idClient, int idProduit){
    for(int i=0; i< this->nbLocations(); i++){
        if(this->_locations[i]._idClient == idClient && this->_locations[i]._idProduit == idProduit){
            std::swap(this->_locations[i], this->_locations[this->nbLocations()-1]);
            this->_locations.pop_back();
            return;
        }
    }
    throw std::string("erreur: le produit "+std::to_string(idProduit)+" n'est pas loué");
}


bool Magasin::trouverProduitDansLocation(int idProduit){
    for(int i=0; i<this->nbLocations(); i++){
        if(this->_locations[i]._idProduit == idProduit)
            return true;
    }
    return false;
}
std::vector<int> Magasin::calculerProduitLibres(){
    std::vector<int> lib;
    for(int i=0; i <this->nbProduits()-1; i++){
        if(!this->trouverProduitDansLocation(this->_produits[i].getId())){
            lib.push_back(this->_produits[i].getId());
        }
    }
    return lib;
}


bool Magasin::trouverClientDansLocation(int idClient){
    for(int i=0; i<this->nbLocations(); i++){
        if(this->_locations[i]._idClient == idClient)
            return true;
    }
    return false;
}
std::vector<int> Magasin::calculerClientLibres(){
    std::vector<int> lib;
    for(int i=0; i <this->nbClients()-1; i++){
        if(!this->trouverClientDansLocation(this->_clients[i].getId())){
            lib.push_back(this->_clients[i].getId());
        }
    }
    return lib;
}

bool Magasin::clientPresent(int idClient){
    for(int i=0; i<this->_clients.size(); i++)
        if(this->_clients[i].getId() == idClient)
            return true;
    return false;
}
