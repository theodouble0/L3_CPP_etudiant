#include <iostream>

#include "Produit.hpp"

Produit::Produit(int id, const std::string &description):
    _id(id), _description(description){

}

int Produit::getId() const{
    return this->_id;
}

std::string Produit::getDescription() const{
    return this->_description;
}

void Produit::afficherProduit() const{
    std::cout << "Client (" << this->getId() << ", " << this->getDescription() <<  ")" << std::endl;
}
