#ifndef MAGASIN_HPP
#define MAGASIN_HPP

#include <iostream>
#include <vector>

#include "Client.hpp"
#include "Produit.hpp"
#include "Location.hpp"

class Magasin
{
private:
    std::vector<Client> _clients;
    std::vector<Produit> _produits;
    std::vector<Location> _locations;
    int _idCourantClient;
    int _idCourantProduit;
    bool clientPresent(int idClient);
public:
    Magasin();
    int nbClients() const;
    void ajouterClient(const std::string & nom);
    void afficherClients() const;
    void supprimerClient(int idClient);

    int nbProduits() const;
    void ajouterProduit(const std::string & produit);
    void afficherProduits() const;
    void supprimerProduit(int idProduit);


    int nbLocations() const;
    void ajouterLocation(int idClient, int idProduit);
    void afficherLocations() const;
    void supprimerLocation(int idClient, int idProduit);
    bool trouverProduitDansLocation(int idProduit);
    std::vector<int> calculerProduitLibres();
    bool trouverClientDansLocation(int idClient);
    std::vector<int> calculerClientLibres();
};

#endif // MAGASIN_HPP

