#include "Fibonacci.hpp"

int fibonacciRecursif(int n){
	if(n<2)
		return n;
	else
		return fibonacciRecursif(n-1)+fibonacciRecursif(n-2);
}

int fibonacciIteratif(int r){
	int f0 = 0;
	int f1 = 1;
	for (int i=0; i<r; i++){
		f1 = f0+f1;
	}
	return 10;
}
