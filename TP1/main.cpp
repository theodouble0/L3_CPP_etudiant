//g++ main.cpp -o main
//g++ -o main.out main.cpp Fibonacci.hpp
#include <iostream>
#include "Fibonacci.hpp"

int main() {
	std::cout << "Hello world !" << std::endl;
	std::cout << "fibonacciIteratif : " << fibonacciIteratif(7) << std::endl;
	std::cout << "fibonacciRecursif : " << fibonacciRecursif(7) << std::endl;
	return 0;
}

/*compilation séparée
 * g++ -c main.cpp
 * g++ -c Fibonacci.cpp
 * g++ main.o Fibonacci.o -o main.out
 * 
 * il faut relancer la 1 et 2
 */
